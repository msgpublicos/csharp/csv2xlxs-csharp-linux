# README

Este archivo README.md contiene la Descripción de la aplicación y los pasos para contener todos los paquetes nuget necesarios
mas los pasos de compilación autocontenida.
App de consola que permite cambiar un csv a un xlsx

## Indice de contenidos
* Descripción de la aplicación
* Descripción de parámetros
* Número de Versión
* Paquetes nuget necesarios
* Como compilar el proyecto
* Como ejecutar el programa

### Descripción de la aplicación
El objetivo a cumplir de esta aplicación es transformar un archivo csv (comma separated ) en un archivo xlsx (2007+) basado en xml.
Para lograr el objetivo se necesita el paquete nuget ClosedXml que permite trabajar con archivos Office con formato xml de manera fácil y ágil.

La documentación del paquete nuget se encuentra siguiente [este link][1]

### Descripción de parámetros
La aplicación tiene definida una serie de parámetros que pueden ser accedidos o listados con --Help

Listado de parámetros:
| Corto | Largo    |          | Descripción del parámetro
|-      |-         |-         |-|
|-h     | --Help   |          | Este listado de parámetros
|-i     | --Input  |[archivo] | Archivo de ingreso en formato CSV
|-o     | --Output |[archivo] | Archivo de salida en formato xlsx
|-f     | --Formato|[?]       | Símbolos separador de campos del archivo csv. **Default [,]**
|-v     | --Version|          | Version del ensamblado
|-s     | --Hoja   |          | Nombre de la hoja en la planilla


### Número de Versión
* Alpha 1.0.1 (20230109)
* Alpha 1.0.0 (20211230)


### Paquetes nuget necesarios
Para que funcione el proyecto se debe instalar el paquete 
    **ClosedXML --version 0.95.4**

Existen las siguientes opciones:
1. Abrir la consola en el directorio del aplicativo y lanzar la siguiente instrucción:
    ``` 
    dotnet add package ClosedXML --version 0.95.4
    ``` 
2. Abrir la consola en el directorio del aplicativo y lanzar la siguiente instrucción:
    ``` 
    dotnet restore csv2xlsx-csharp-linux.sln
    ``` 

### Como compilar el proyecto

``` 
dotnet publish "/home/marcos/Desarrollo/dotnet/csv2xlsx-csharp-linux/csv2xlsx-csharp-linux.sln" -c release -r linux-x64 --self-contained  -p:PublishTrimmed=true -o:release/ -p:PublishSingleFile=true
``` 


### Como ejecutar el programa
Ejecutar el siguiente comando:

``` 
 ./csv2xlsx.csharp -h
```

[1]:(https://www.nuget.org/packages/ClosedXML/)