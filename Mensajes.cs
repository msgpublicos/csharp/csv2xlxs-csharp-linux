using System.Reflection;

public static class Mensajes
{
    public static void MensajeHelp()
    {
        Console.WriteLine("Listado de parametros");
        Console.WriteLine("Ejemplo de uso: csv2xlsx.csharp -i:nombrearchivo.csv -f:; -o:salida.xlsx");
        Console.WriteLine("Ejemplo de uso: csv2xlsx.csharp --Help");
        Console.WriteLine("");
        Console.WriteLine("-h, --Help             : Este listado de parametros");
        Console.WriteLine("-i, --Input  [archivo] : Archivo de ingreso en formato CSV");
        Console.WriteLine("-o, --Output [archivo] : Archivo de salida en fortmato xlsx");
        Console.WriteLine("-v, --Version          : Version del ensamblado");
        Console.WriteLine("-s, --Hoja             : Nombre para la hoja de la planilla, por defecto [Hoja]");
        Console.WriteLine("-f, --Formato          : Separador de columnas, por defecto [,]");
    }
    public static void MensajeError()
    {
        Console.WriteLine($"No se han pasado los parametros iniciales obligatorios (--Input)\n");
        Console.WriteLine();
    }

    public static void MensajeVersion()
    {
        Assembly? thisAssem = typeof(Mensajes).Assembly;
        AssemblyName? thisAssemName = thisAssem.GetName();

        string? ver = thisAssemName.Version == null ? new Version("0").ToString() : thisAssemName.Version.ToString();

        Console.WriteLine("La version del ensamblado csv2xlsx.csharp es 1.0.1");

    }

    public static void MensajeErrorParametroInput()
    {
        Console.WriteLine($"El parametro --Input fue mal ingresado \n");
        Console.WriteLine();
    }

    public static void MensajeErrorParametroOutput()
    {
        Console.WriteLine($"El parametro --Output fue mal ingresado \n");
        Console.WriteLine();
    }

    public static void MensajeErrorArchivoCsv(string sRuta)
    {
        Console.WriteLine($"No existe el archivo csv {sRuta}\n");
        Console.WriteLine();
    }

}
