using System.Diagnostics;
using System.Text.RegularExpressions;
using ClosedXML.Excel;

public static class Transformacion
{
    public static void TransformaCsvXlsx(string sArchivoCsv, string sArchivoXlsx, string sSeparador, string sSheet)
    {
        var workbook = new XLWorkbook();
        var worksheet = workbook.Worksheets.Add(sSheet);
        worksheet.Cell(1, 1).Value = "csv2xlsx.csharp.linux";
        using (StreamReader oReader = new StreamReader(sArchivoCsv))
        {
            string? line;
            int iLinea = 1;

            while ((line = oReader.ReadLine()) != null)
            {
                Debug.WriteLine($"Texto linea : {line}");

                int iCols = 1;
                string[] aArregloLinea = line.Split(sSeparador);

                foreach (string sCelda in aArregloLinea)
                {
                    worksheet.Cell(iLinea, iCols).Value = sCelda;
                    iCols++;
                }
                iLinea++;
            }
        }

        workbook.SaveAs(sArchivoXlsx);

    }
    public static bool ValidaExtensionArchivo(string sArchivo, string sExtension)
    {
        if (Path.HasExtension(sArchivo))
        {
            if (Path.GetExtension(sArchivo).ToLower() == sExtension.ToLower())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public static string CrearNombreXlsx(string sArchivo)
    {
        string sFullPath = Path.GetFullPath(sArchivo).Replace(Path.GetFileName(sArchivo), "");
        string sFullName = Path.GetFileName(sArchivo).Replace(Path.GetExtension(sArchivo), "") + ".xlsx";

        Debug.WriteLine($"Nombre del xlsx: {sFullPath + sFullName}");

        return sFullPath + sFullName;
    }
}
