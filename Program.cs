﻿
using System.Diagnostics;
string sSeparador = ",";
string sNombreArchivoCsv = "archivo.csv";
string sNombreArchivoXlsx = "archivo.xlsx";
string sSheet = "Hoja";

if (args.Count() == 0)
{
    Mensajes.MensajeError();
    Mensajes.MensajeHelp();
    return;
}
else
{
    if (Array.Find(args, p => p.ToLower().Contains("-h")) != null || Array.Find(args, p => p.ToLower().Contains("--help")) != null)
    {
        Mensajes.MensajeHelp();
        return;
    }

    if (Array.Find(args, p => p.ToLower().Contains("-v")) != null || Array.Find(args, p => p.ToLower().Contains("--version")) != null)
    {
        Mensajes.MensajeVersion();
        return;
    }

    if (Array.Find(args, p => p.ToLower().Contains("-s")) != null || Array.Find(args, p => p.ToLower().Contains("--hoja")) != null)
    {
        string? sValor = "";

        if (Array.Find(args, p => p.ToLower().Contains("-s")) != null)
        {
            sValor = Array.Find(args, p => p.ToLower().Contains("-s"));
        }
        else
        {
            sValor = Array.Find(args, p => p.ToLower().Contains("--hoja"));
        }
        if (!String.IsNullOrEmpty(sValor))
        {
            string[] aNA = sValor.Split(":");
            if (aNA.Count() == 2)
            {
                sSheet = aNA[1];
                Debug.WriteLine($"Nombre de la hoja: {sSheet}");
            }
        }
    }

    if (Array.Find(args, p => p.ToLower().Contains("-f")) != null || Array.Find(args, p => p.ToLower().Contains("--formato")) != null)
    {
        string? sValor = "";

        if (Array.Find(args, p => p.ToLower().Contains("-f")) != null)
        {
            sValor = Array.Find(args, p => p.ToLower().Contains("-f"));
        }
        else
        {
            sValor = Array.Find(args, p => p.ToLower().Contains("--formato"));
        }
        if (!String.IsNullOrEmpty(sValor))
        {
            string[] aNA = sValor.Split(":");
            if (aNA.Count() == 2)
            {
                sSeparador = aNA[1];
                Debug.WriteLine($"Nombre de la hoja: {sSheet}");
            }
        }
    }


    if (Array.Find(args, p => p.ToLower().Contains("-i")) != null || Array.Find(args, p => p.ToLower().Contains("--input")) != null)
    {
        string? sValor = "";

        if (Array.Find(args, p => p.ToLower().Contains("-i")) != null)
        {
            sValor = Array.Find(args, p => p.ToLower().Contains("-i"));
        }
        else
        {
            sValor = Array.Find(args, p => p.ToLower().Contains("--input"));
        }
        if (!string.IsNullOrEmpty(sValor))
        {
            string[] aNA = sValor.Split(":");

            if (aNA.Count() == 2)
            {
                sNombreArchivoCsv = aNA[1];
                Debug.WriteLine($"Archivo entrada: {sNombreArchivoCsv}");

                if (string.IsNullOrWhiteSpace(sNombreArchivoCsv))
                {
                    Mensajes.MensajeErrorParametroInput();
                    return;
                }
            }
            else
            {
                Mensajes.MensajeErrorParametroInput();
                return;
            }
        }
        else
        {
            Mensajes.MensajeErrorParametroInput();
            return;
        }
    }
    else
    {
        Mensajes.MensajeError();
        Mensajes.MensajeHelp();
        return;
    }

    if (Array.Find(args, p => p.ToLower().Contains("-o")) != null || Array.Find(args, p => p.ToLower().Contains("--output")) != null)
    {
        string? sValor = "";

        if (Array.Find(args, p => p.ToLower().Contains("-o")) != null)
        {
            sValor = Array.Find(args, p => p.ToLower().Contains("-o"));
        }
        else
        {
            sValor = Array.Find(args, p => p.ToLower().Contains("--output"));
        }
        if (!string.IsNullOrEmpty(sValor))
        {
            string[] aNA = sValor.Split(":");
            if (aNA.Count() == 2)
            {
                sNombreArchivoXlsx = Transformacion.CrearNombreXlsx(aNA[1]);
            }
            else
            {
                Mensajes.MensajeErrorParametroOutput();
                return;
            }
            Debug.WriteLine($"Nombre del archivo xlsx: {sNombreArchivoXlsx}");
        }
        else
        {
            Mensajes.MensajeErrorParametroOutput();
            return;
        }
    }
    else
    {
        sNombreArchivoXlsx = Transformacion.CrearNombreXlsx(sNombreArchivoCsv);
    }

    // Validar nombres de archivo
    if (Transformacion.ValidaExtensionArchivo(sNombreArchivoCsv, ".csv"))
    {
        Debug.WriteLine($"Nombre del csv : {sNombreArchivoCsv}");
        // Validar existencia de archivos        
        if (File.Exists(sNombreArchivoCsv))
        {
            // Lectura del Archivo CSV
            Transformacion.TransformaCsvXlsx(sNombreArchivoCsv, sNombreArchivoXlsx, sSeparador, sSheet);
        }
        else
        {
            Mensajes.MensajeErrorArchivoCsv(sNombreArchivoCsv);
            return;
        }
    }
    else
    {
        Mensajes.MensajeErrorParametroInput();
        return;
    }
}

